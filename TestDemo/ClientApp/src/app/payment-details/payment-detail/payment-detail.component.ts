import { Component, OnInit } from '@angular/core';
import { PaymentDetailService } from 'src/app/shared/payment-detail.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styles: []
})
export class PaymentDetailComponent implements OnInit {

  constructor(public service: PaymentDetailService, ) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {

    if (form != null)
      form.resetForm()
    this.service.formData = {
      PMId: 0,
      CardOwnerName: '',
      CardNumber: '',
      ExpirationDate: '',
      CVV: ''
    }
  }

  onSubmit(form: NgForm) {
    if(this.service.formData.PMId == 0)
    {
      this.insertRecord(form)
    }
    else{
      this.updateRecord(form)
    }

  }

  insertRecord(form: NgForm)
  {
    this.service.postPaymenDetail().subscribe(
      res => { this.resetForm();  this.service.refreshList() },
      err => { console.log(err) }
    )
  }

  updateRecord(form: NgForm)
  {
    this.service.putPaymenDetail().subscribe(
      res => { this.resetForm(); this.service.refreshList() },
      err => { console.log(err) }
    )
  }

}
