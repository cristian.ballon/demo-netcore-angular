import { Injectable, Inject } from '@angular/core';
import {PaymentDetail} from './payment-detail.model'
import { HttpClient, HttpHeaders } from "@angular/common/http";



  @Injectable({
    providedIn: 'root'
  })
export class PaymentDetailService {
  formData: PaymentDetail
  baseUrl : string
  list: PaymentDetail[]

    constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

        this.baseUrl = baseUrl

    }

  postPaymenDetail()
  {
      return this.http.post(this.baseUrl + 'api/PaymentDetail', this.formData)
  }


  putPaymenDetail()
  {
      return this.http.put(this.baseUrl + 'api/PaymentDetail/' + this.formData.PMId, this.formData)
  }


  refreshList(){
      return this.http.get(this.baseUrl + 'api/PaymentDetail')
    .toPromise()
    .then(res => { this.list = res as PaymentDetail[]})
  }

    deletePaymentDetail(id) {
        return this.http.delete(this.baseUrl + 'api/PaymentDetail/' + id);
    }


}
